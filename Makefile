#============================================================================
OUT_NAME     := uart_test
#EXEC_EXT    := .exe
#OUT_DIR     := .
#CLEAN_DIR   := $(OUT_DIR)/tmp
#CLEAN_FILES := "$(OUT_DIR)/$(OUT_NAME).map" "$(OUT_DIR)/$(OUT_NAME).exe"
#----------------------------------------------------------------------------

# 1-st way to select source files
SRCS := uart_test.c uart.c
HDRS := uart.h

# 2-nd way to select source files
#SRC_DIRS := .
#HDR_DIRS := .

#----------------------------------------------------------------------------
#INC_DIRS  := ../libs/include 
#INC_FLAGS := -I/usr/include/foo -I `wx-config --cxxflags`
#DEFS      := -DUNIT_DEBUG
OPTIM      := -Os -fomit-frame-pointer
WARN       := -Wall
#
#ASFLAGS   := -mcpu=cortex-m3 -mthumb $(ASFLAGS)
#CFLAGS    := $(WARN) $(OPTIM) $(DEFS) $(CFLAGS) -pipe
#CXXFLAGS  := `wx-config --cxxflags` $(CXXFLAGS) $(CFLAGS)
#LDFLAGS   := `wx-config --libs` -lm -lrt -lpthread $(LDFLAGS)
#
#PREFIX    := /opt
#----------------------------------------------------------------------------
#_AS  := @as
#_CC  := @gcc
#_CXX := @g++
#_LD  := @g++
#----------------------------------------------------------------------------
include Makefile.skel
#============================================================================
