/*
 * Simple UART Linux wrapper based on <termios.h>
 * File: "uart.c"
 */

//----------------------------------------------------------------------------
#include "uart.h"
//----------------------------------------------------------------------------
#include <string.h>
#include <errno.h>
//----------------------------------------------------------------------------
#ifdef UART_USE_POLL
//#include <poll.h>       // poll()
#  include <sys/poll.h>   // poll()
#else
#  include <sys/select.h> // select() [POSIX way]
#endif
//----------------------------------------------------------------------------
static int uart_speed(int speed)
{
  if      (speed ==      0) return B0;
  else if (speed <=     50) return B50;
  else if (speed <=     75) return B75;
  else if (speed <=    110) return B110;
  else if (speed <=    134) return B134;
  else if (speed <=    150) return B150;
  else if (speed <=    200) return B200;
  else if (speed <=    300) return B300;
  else if (speed <=    600) return B600;
  else if (speed <=   1200) return B1200;
  else if (speed <=   1800) return B1800;
  else if (speed <=   2400) return B2400;
  else if (speed <=   4800) return B4800;
  else if (speed <=   9600) return B9600;
  else if (speed <=  19200) return B19200;
  else if (speed <=  38400) return B38400;
  else if (speed <=  57600) return B57600;
  else if (speed <= 115200) return B115200;
  else if (speed <= 230400) return B230400;
  else                      return UART_DEFAULT_SPEED;
}
//----------------------------------------------------------------------------
// open UART (return file descriptor or error code < 0)
int uart_open(uart_t *self,
              const char *device, // tty device like "/dev/ttyUSB0"
	      int speed,          // UART speed 50..300..9600..115200..230400
              int timeout)        // timeout [0.1s]
{
  strncpy(self->device, device, UART_DEVICE_LEN);
  self->device[UART_DEVICE_LEN - 1] = '\0';

  // open device
  self->fd = open(device, O_RDWR | O_NOCTTY);
  if (self->fd < 0)
  {
    fprintf(stderr, "error: can't open '%s' in uart_open(): %s\n",
	    device, strerror(errno));
    return -1;
  }

  // save current UART settings
  tcgetattr(self->fd, &self->tio_saved);

  // set new UART settings
  bzero(&self->tio, sizeof(self->tio));
  self->tio.c_cflag = uart_speed(speed) | CLOCAL | CREAD | CS8; // 8N1
  self->tio.c_iflag = IGNPAR; // ignore parity error
  self->tio.c_oflag = 0;      // raw output
  self->tio.c_lflag = 0;      // no ICANON, no echo

  self->tio.c_cc[VTIME] = timeout; // inter-character timer
  self->tio.c_cc[VMIN]  = 1;       // blocking read until 1 character arrives
  
  tcflush(self->fd, TCIFLUSH);
  tcsetattr(self->fd, TCSANOW, &self->tio);

  return self->fd;
}
//----------------------------------------------------------------------------
// close UART
void uart_close(uart_t *self)
{
  // restore old UART settings
  tcsetattr(self->fd, TCSANOW, &self->tio_saved);
  tcflush(self->fd, TCIFLUSH);
  close(self->fd);
}
//----------------------------------------------------------------------------
// select wraper for non block read (return 0:false, 1:true, <0:error code)
int uart_select(const uart_t *uart, int msec)
{
  int retv, fd = uart->fd;

#ifdef UART_USE_POLL
  // use poll()
  struct pollfd fds[1];

  while (1)
  {
    fds->fd      = fd;
    fds->events  = POLLIN;
    fds->revents = 0;

    retv = poll(fds, 1, msec);
    if (retv < 0)
    {
      if (errno == EINTR)
          continue; // interrupt by signal

      fprintf(stderr, "error: poll('%s', %i) return %i in uart_select(): %s\n",
              uart->device, msec, retv, strerror(errno));

      return -1; // error
    }

    break;
  }

  if (retv > 0)
  {
    if (fds->revents & (POLLERR | POLLHUP | POLLNVAL))
    {
      fprintf(stderr, "error in uart_select('%s', %i)\n",
              uart->device, msec);

      return -1; // error
    }

    if (fds->revents & (POLLIN | POLLPRI))
      return 1; // may non block read
  }

  return 0; // pipe is empty

#else
  // use select()
  fd_set fds;
  struct timeval to, *pto;

  while (1)
  {
    FD_ZERO(&fds);
    FD_SET(fd, &fds);
    if (msec >= 0)
    {
      to.tv_sec  = msec / 1000;
      to.tv_usec = (msec % 1000) * 1000;
      pto = &to;
    }
    else // if msec < 0 then wait forewer
      pto = (struct timeval *) NULL;

    retv = select(fd + 1, &fds, NULL, NULL, pto);
    if (retv < 0)
    {
      if (errno == EINTR)
          continue; // interrupt by signal
      
      fprintf(stderr, "error: select('%s', %i) return %i in uart_select(): %s\n",
              uart->device, msec, retv, strerror(errno));

      return -1; // error
    }
    
    break;
  } // while(1)

  if (FD_ISSET(fd, &fds))
    return 1; // may non block read

  return 0; // pipe is empty
#endif
}
//----------------------------------------------------------------------------
// read from UART
int uart_read(const uart_t *uart, void *buf, int size)
{
  if (size > 0)
    while (1)
    {
      size = read(uart->fd, buf, size);
      if (size < 0)
      {
        if (errno == EINTR)
          continue; // interrupt by signal

	fprintf(stderr, "error: can't read from '%s' in uart_read(): %s\n",
	        uart->device, strerror(errno));

	return -1;
      }

      return size;
    } // while (1)

  return 0;
}
//----------------------------------------------------------------------------
// read from UART at once
int uart_read_all(const uart_t *uart, void *buf, int size)
{
  int retv, cnt = 0, fd = uart->fd;
  char *ptr = (char*) buf;

  while (size > 0)
  {
    retv = read(fd, (void*) ptr, size);
    if (retv < 0)
    {
      if (errno == EINTR)
        continue; // interrupt by signal
      
      fprintf(stderr, "error: can't read from '%s' in uart_read_all(): %s\n",
              uart->device, strerror(errno));
      
      return -1;
    }
    else if (retv == 0)
      return cnt;

    ptr  += retv;
    cnt  += retv;
    size -= retv;
  }
  return cnt;
}
//----------------------------------------------------------------------------
// read from UART at once with timeout
int uart_read_to(const uart_t *uart, void *buf, int size, int msec)
{
  int retv, cnt = 0, fd = uart->fd;
  char *ptr = (char*) buf;

  while (size > 0)
  {
    retv = uart_select(uart, msec);
    if (retv < 0)
    {
      if (errno == EINTR)
        continue; // interrupt by signal
      
      return -1;
    }

    if (retv == 0)
      break; // timeout

    retv = read(fd, (void*) ptr, size);
    if (retv < 0)
    {
      if (errno == EINTR)
        continue; // interrupt by signal

      fprintf(stderr, "error: can't read from '%s' in uart_read_to(): %s\n",
              uart->device, strerror(errno));

      return -2;
    }
    else if (retv == 0)
      break; // ?!

    ptr  += retv;
    cnt  += retv;
    size -= retv;
  }

  return cnt;
}
//----------------------------------------------------------------------------
// write to UART
int uart_write(const uart_t *uart, const void *buf, int size)
{
  int retv = write(uart->fd, buf, size);
  if (retv < 0)
  {
    fprintf(stderr, "error: can't write to '%s' in uart_write(): %s\n",
	    uart->device, strerror(errno));

    return -1;
  }

  return retv;
}
//----------------------------------------------------------------------------

/*** end of "uart.c" file ***/

