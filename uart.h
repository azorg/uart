/*
 * Simple UART Linux wrapper based on <termios.h>
 * File: "uart.h"
 */

#ifndef UART_H
#define UART_H
//----------------------------------------------------------------------------
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
//----------------------------------------------------------------------------
// use poll() nor select() in uart_select()
#define UART_USE_POLL
//----------------------------------------------------------------------------
#define UART_DEVICE_LEN 80
#define UART_DEFAULT_SPEED B9600
#define UART_8N1 CS8
//----------------------------------------------------------------------------
// component data structure
typedef struct uart_ {
  struct termios tio_saved;
  struct termios tio;
  char device[UART_DEVICE_LEN];
  int fd;
} uart_t;
//----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
//----------------------------------------------------------------------------
// open UART (return file descriptor or error code < 0)
int uart_open(uart_t *self,
              const char *device, // tty device like "/dev/ttyUSB0"
	      int speed,          // UART speed 50..300..9600..115200..230400
              int timeout);       // timeout [0.1s]
//----------------------------------------------------------------------------
// close UART
void uart_close(uart_t *self);
//----------------------------------------------------------------------------
// select wraper for non block read (return 0:false, 1:true, <0:error code)
int uart_select(const uart_t *self, int msec);
//----------------------------------------------------------------------------
// read from UART
int uart_read(const uart_t *uart, void *buf, int size);
//----------------------------------------------------------------------------
// read from UART at once
int uart_read_all(const uart_t *uart, void *buf, int size);
//----------------------------------------------------------------------------
// read from UART at once with timeout
int uart_read_to(const uart_t *uart, void *buf, int size, int msec);
//----------------------------------------------------------------------------
// write to UART
int uart_write(const uart_t *uart, const void *buf, int size);
//----------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif // __plusplus
//----------------------------------------------------------------------------
#endif // UART_H

/*** end of "uart.h" file ***/

