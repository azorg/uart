/*
 * Simple UART Linux wrapper based on <termios.h>
 * File: "uart_test.h" - test module
 */

//-----------------------------------------------------------------------------
#include <string.h>
#include "uart.h"
//-----------------------------------------------------------------------------
#define DEVICE "/dev/ttyUSB0"
#define BAUDRATE 9600
#define BUF_SIZE 256
//-----------------------------------------------------------------------------
int main()
{
  char msg1[] = "Hello world!";
  char msg2[] = "I am Alex Zorg.";
  uart_t uart;
  char buf[BUF_SIZE];
  int retv, cnt;

  // open (1)
  retv = uart_open(&uart, DEVICE, BAUDRATE, 1);
  printf("\nuart_open('%s') return %i\n", DEVICE, retv);

  // write (1)
  retv = uart_write(&uart, msg1, strlen(msg1));
  printf("uart_write('%s', '%s') return %i\n", DEVICE, msg1, retv);

  if (retv < 0)
    return -1;

  // read (1)
  cnt = 0;
  while (cnt < strlen(msg1))
  {
    retv = uart_read(&uart, buf + cnt, BUF_SIZE - 1 - cnt);
    printf("uart_read('%s', %i) return %i\n", DEVICE, BUF_SIZE - 1 - cnt, retv);
    if (retv > 0)
    {
      cnt += retv;
      buf[cnt] = '\0';
      printf("buf='%s', cnt=%i\n", buf, cnt);
    }
  }

  // close (1)
  uart_close(&uart);

  // open (2)
  retv = uart_open(&uart, DEVICE, BAUDRATE, 1);
  printf("\nuart_open('%s') return %i\n", DEVICE, retv);

  // select (2)
  retv = uart_select(&uart, 0);
  printf("uart_select('%s', 0) return %i\n", DEVICE, retv);
  
  retv = uart_select(&uart, 1000);
  printf("uart_select('%s', 1000) return %i\n", DEVICE, retv);

  // write (2)
  retv = uart_write(&uart, msg2, strlen(msg2));
  printf("uart_write('%s', '%s') return %i\n", DEVICE, msg2, retv);

  // select (2)
  retv = uart_select(&uart, 10);
  printf("uart_select('%s', 10) return %i\n", DEVICE, retv);

  // read (2)
  retv = uart_read_to(&uart, buf, BUF_SIZE - 1, 100);
  printf("uart_read_to('%s', %i, 100) return %i\n", DEVICE, BUF_SIZE - 1, retv);
  if (retv > 0)
  {
    buf[retv] = '\0';
    printf("buf='%s', retv=%i\n", buf, retv);
  }

  
  // close (2)
  uart_close(&uart);

  return 0;
}
//-----------------------------------------------------------------------------

/*** end of "uart_test.c" file ***/

